Source: ruby-dbus
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Paul van Tilburg <paulvt@debian.org>,
           Utkarsh Gupta <utkarsh@debian.org>
Section: ruby
Testsuite: autopkgtest-pkg-ruby
Priority: optional
Build-Depends: dbus,
               debhelper-compat (= 13),
               gem2deb,
               ruby-rspec
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-dbus
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-dbus.git
Homepage: https://github.com/mvidner/ruby-dbus
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-dbus
Architecture: all
Depends: ruby:any | ruby-interpreter,
         ${misc:Depends}
Recommends: ruby-glib2
Suggests: ruby-nokogiri
Description: Ruby module for interaction with D-Bus
 This module allows Ruby programs to interface with the D-Bus message
 bus installed on newer Unix operating systems.
 .
 Ruby D-Bus currently supports the following features:
   * Connecting to local buses.
   * Accessing remote services, objects and interfaces.
   * Invoking methods on remote objects synchronously and asynchronously.
   * Catch signals on remote objects and handle them via callbacks.
   * Remote object introspection.
   * Walking object trees.
   * Creating services and registering them on the bus.
   * Exporting objects with interfaces on a bus for remote use.
   * Rubyish D-Bus object and interface syntax support that automatically
     allows for introspection.
   * Emitting signals on exported objects.
